function obtenerUsuarios(){
    var listaUsuarios = JSON.parse(localStorage.getItem('listaUsuariosLS'));

    if(listaUsuarios == null){
        listaUsuarios = [
            ['1', 'Jose Adrian Campos', 'adrian@outlook.com', '12345', '1'],
            ['1', 'William ', 'will@outlook.com', '12345', '1']
        ]
    }
    return listaUsuarios
}

function validarCredenciales(correo, pass){
    var listaUsuarios = obtenerUsuarios();
    var acceso = false;

    for(i = 0; i < listaUsuarios.length; i++){
        if(correo == listaUsuarios[i][2] && correo == listaUsuarios[i][3]){
            acceso = true;
            sessionStorage.setItem('usuarioActivo', listaUsuarios[i][1]);
            
        }
    }

    return acceso;
}