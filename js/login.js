var formulario = document.querySelector("#formulario");

formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    var correo = '';
    var pass = '';
    var acceso = false;

    correo = document.querySelector("#correo").value;
    pass = document.querySelector("#pass").value;

    acceso = validarCredenciales(correo, pass);

    if(acceso){
        ingresar();
    }
})

function ingresar(){
    
    window.location.href = 'reservaciones.html';
}